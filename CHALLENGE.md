# Thriflora

- Thriflora es un empresa que se dedica a suministrar plantas a viveros de todo el mundo desde el año 1988.
- Desde el principio, siempre se ha dedicado a la venta a empresas, pero desde hace tiempo están pensando en hacer venta directa online a cualquier persona.

## Problema

- El equipo de desarrollo ya está trabajando en montar una tienda online pero todavía quedan un par de meses hasta que este disponible. Mientras tanto el equipo de marketing ha pensado que sería interesante hacer una página para poder ir recogiendo los emails de potenciales clientes que quieran ser los primeros en probar la tienda online, que por el momento solo se va abrir en España, Francia e Italia.

## Solución

- Los equipos de desarrollo y marketing han acordado que van a montar una web para que los clientes potenciales pueden dejar sus datos.
El equipo de marketing quiere que los datos se registren en un servicio externo, pero todavía no han decidido cual, así que han pedido al equipo de desarrollo que de momento se centren solo en la web con el formulario y que se despreocupen de momento de meter ningún tipo de base de datos.

## Requisitos

- Los usuarios introducen su email en un campo de texto.
- Los usuarios seleccionan su país de residencia en un selector.
- El selector solo muestra los paises donde se va a abrir la tienda: España, Francia e Italia.
- Los usuarios aceptan recibir el email usando un checkbox "Quiero recibir un email cuando la tienda esté disponible".
- Los usuarios se registran haciendo click en el botón "¡Quiero ser la primera en comprar!".
- El botón para apuntarse solo se habilita cuando el email es válido, hay un país seleccionado y el checkbox está marcado.
- Cuando el usuario se ha registrado correctamente, aparece un mensaje de registro completado: "¡Perfecto, te avisaremos la primera!"
