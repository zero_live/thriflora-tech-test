# TASKS

## Clean up (preparation)

- [x] Copy template proyect
- [x] Clean template by removing API
- [x] Clean template APP by removing TableComponent and AppService
- [x] Change make commands for sh scripts
- [x] Update README (Reemplace make with scripts)
- [x] **Remove Makefile**
- [x] Push to public repository

## Creating form (MVP)

- [x] Create registration button (Text in challenge)
- [x] **Add hidden registration message (Text in challenge)**
- [x] Show registration message on click button
- [x] Create countries select (Countries in challenge, starts with a selected country)
- [x] Create acceptance checkbox (Text in challenge)
- [x] Disable button until acceptance checkbox is not checked
- [x] Create buyer email input
- [x] Disable registration button while email is empty
- [x] Disable registration button while email is invalid
- [x] Create integration tests
    - [x] **when the fields are not filled can not register**
    - [x] **does not show the registration message**
    - [x] **when the fields are filled can register**
- [x] Create pipeline for CI
- [x] **Create pipeline for CD**
- [x] Add some styles

## Improvements

- [x] Extract countries select to its own component
- [x] Extract registration message to its own component
- [x] Extract registration button to its own component
  - [x] Create unit tests
- [x] Extract acceptance checkbox to its own component
  - [x] Create unit tests
- [x] Extract email input to its own component
  - [x] Create unit tests

## TO TEN

- [ ] Add text for required fields in red
- [x] When the email input is empty highligth it in red
  - [ ] Test it
- [x] When the email input is invalid highligth it in red
  - [x] Test it
- [ ] ~~When the acceptance checkbox is unchecked highligth it in red~~
  - [ ] ~~Test it~~
- [ ] Pass available country list from App to select
- [ ] Retrieve country list from Countries service
- [ ] Add more styles
