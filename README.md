# THRIFLORA

This project is a tech test for Thriflora junior developer in AngularJS. I copied the requirements to `CHALLENG.md` for have a estable version of the test.

I have a plan to complete this tech test, you can found it in `TASKS.md`, emergent tasks are in bold. I go to expend 5 pomodoros in this project, we can talk about all undone tasks.
> I used a base project for my firsts tasks are about cleaning it.

## System requirements

- `Docker version 20` or compatible.
- `docker-compose version 1.29` or compatible.

## How to run the project

You have to run the following script: `sh scripts/up-in-background.sh` with this you can open the proyect in your browser in the URL `localhost:4200`.

### For run the test

- If you want to run only the unit tests use: `sh scripts/app-test-unit.sh`
- If you want to run all the tests use: `sh scripts/app-test-all.sh`
