import { RegistrationButtonComponent } from './registration-button.component';
import {render, screen } from '@testing-library/angular'
import userEvent from '@testing-library/user-event'

describe('RegistrationButtonComponent', () => {
  let component: any

  beforeEach( async() => {
    component = await render(RegistrationButtonComponent, {
      componentProperties: {
        enabled: true
      }
    })
  })

  it('emits that has been clicked', async () => {
    const registrationButton = await screen.queryByText('¡Quiero ser la primera en comprar!')
    let hasBeenClicked = false
    const callback = () => { hasBeenClicked = true }
    component.fixture.componentInstance.registrationClickedEvent.subscribe(callback)

    await userEvent.click(registrationButton as HTMLElement)

    expect(hasBeenClicked).toBeTruthy()
  })
})
