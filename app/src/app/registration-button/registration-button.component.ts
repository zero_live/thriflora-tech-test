import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-registration-button',
  templateUrl: './registration-button.component.html',
  styleUrls: ['./registration-button.component.scss']
})

export class RegistrationButtonComponent implements OnInit {
  @Input() enabled: boolean = false
  @Output() registrationClickedEvent = new EventEmitter<string>()

  constructor() { }

  ngOnInit(): void {}

  clicked() {
    this.registrationClickedEvent.emit()
  }

}
