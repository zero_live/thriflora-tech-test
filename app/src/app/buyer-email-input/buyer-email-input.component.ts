import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-buyer-email-input',
  templateUrl: './buyer-email-input.component.html',
  styleUrls: ['./buyer-email-input.component.scss']
})
export class BuyerEmailInputComponent implements OnInit {
  @Output() emilChangedEvent = new EventEmitter<string>()
  @Input() hasValidEmail: boolean = false

  email: string = ""

  constructor() { }

  ngOnInit(): void {
  }

  showError() {
    const isEmpty = (this.email === '')

    return (!this.hasValidEmail && !isEmpty)
  }

  onChange() {
    this.emilChangedEvent.emit(this.email)
  }
}
