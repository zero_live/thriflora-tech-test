import {render, screen } from '@testing-library/angular'
import userEvent from '@testing-library/user-event'
import { FormsModule } from '@angular/forms';

import { BuyerEmailInputComponent } from './buyer-email-input.component';


describe('BuyerEmailInputComponent', () => {
  it('emits that has been changed', async () => {
    const component = await renderBuyerEmailInput({})
    const someEmailAddress = 'some@email.com'
    let inputValue = ''
    const callback = (newValue: string) => { inputValue = newValue }
    const inputEmailBuyer = screen.getByRole('textbox', { hidden: true })
    component.fixture.componentInstance.emilChangedEvent.subscribe(callback)

    await userEvent.type(inputEmailBuyer, someEmailAddress)

    expect(inputValue).toBe(someEmailAddress)
  })

  it('renders without highligth error when is empty', async () => {
    await renderBuyerEmailInput({ hasValidEmail: true })

    const inputEmailBuyer = screen.getByRole('textbox', { hidden: true })

    expect(inputEmailBuyer.classList).not.toContain('on-error')
  })

  it('renders with highligth as error when has an invalid email and is not empty', async () => {
    await renderBuyerEmailInput({ hasValidEmail: false })
    let inputEmailBuyer = screen.getByRole('textbox', { hidden: true })

    await userEvent.type(inputEmailBuyer, 'invalid@@email.duck')

    expect(inputEmailBuyer.classList).toContain('on-error')
  })

  async function renderBuyerEmailInput(properties: object) {
    const component = await render(BuyerEmailInputComponent, {
      detectChanges: true,
      imports: [FormsModule],
      componentProperties: properties
    })

    return component
  }
})
