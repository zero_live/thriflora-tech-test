import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  hasToShowRegistrationMessage: boolean = false
  canRegister: boolean = false
  isAccepted: boolean = false
  buyerEmail: string = ""

  constructor(private service: AppService) {}

  ngOnInit() {}

  onChangeBuyerEmail(email: string) {
    this.buyerEmail = email

    this.checkForEnableRegistration()
  }

  onChangeAcceptance(isAccepted: boolean) {
    this.isAccepted = isAccepted

    this.checkForEnableRegistration()
  }

  checkForEnableRegistration() {
    this.canRegister = (this.isAccepted && this.hasValidEmail())
  }

  showRegistrationMessage() {
    this.hasToShowRegistrationMessage = true
  }

  hasValidEmail() {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.buyerEmail)
  }
}
