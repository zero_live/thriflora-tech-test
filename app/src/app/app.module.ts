import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CountriesSelectComponent } from './countries-select/countries-select.component';
import { RegistrationMessageComponent } from './registration-message/registration-message.component';
import { RegistrationButtonComponent } from './registration-button/registration-button.component';
import { AcceptanceCheckboxComponent } from './acceptance-checkbox/acceptance-checkbox.component';
import { BuyerEmailInputComponent } from './buyer-email-input/buyer-email-input.component';

@NgModule({
  declarations: [
    AppComponent,
    CountriesSelectComponent,
    RegistrationMessageComponent,
    RegistrationButtonComponent,
    AcceptanceCheckboxComponent,
    BuyerEmailInputComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
