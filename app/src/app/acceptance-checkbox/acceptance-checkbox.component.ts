import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-acceptance-checkbox',
  templateUrl: './acceptance-checkbox.component.html',
  styleUrls: ['./acceptance-checkbox.component.scss']
})
export class AcceptanceCheckboxComponent implements OnInit {
  @Output() acceptanceChangedEvent = new EventEmitter<boolean>()

  constructor() { }

  ngOnInit(): void {
  }

  onChange(event: any) {
    const isChecked = event.target.checked

    this.acceptanceChangedEvent.emit(isChecked)
  }
}
