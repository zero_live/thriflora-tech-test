import {render, screen } from '@testing-library/angular'
import userEvent from '@testing-library/user-event'

import { AcceptanceCheckboxComponent } from './acceptance-checkbox.component';

describe('AcceptanceCheckboxComponent', () => {
  let component: any

  beforeEach( async() => {
    component = await render(AcceptanceCheckboxComponent)
  })

  it('emits that has been changed', async () => {
    const acceptanceCheckbox = screen.getByRole('checkbox', { hidden: true })
    let hasBeenClicked = false
    const callback = () => { hasBeenClicked = true }
    component.fixture.componentInstance.acceptanceChangedEvent.subscribe(callback)

    await userEvent.click(acceptanceCheckbox as HTMLElement)

    expect(hasBeenClicked).toBeTruthy()
  })
});
