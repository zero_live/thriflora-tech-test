import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core'
import {render, screen } from '@testing-library/angular'
import { AppComponent } from '../src/app/app.component'
import { RegistrationButtonComponent } from '../src/app/registration-button/registration-button.component'
import { CountriesSelectComponent } from '../src/app/countries-select/countries-select.component'
import { RegistrationMessageComponent } from '../src/app/registration-message/registration-message.component'
import { AcceptanceCheckboxComponent } from '../src/app/acceptance-checkbox/acceptance-checkbox.component'
import { BuyerEmailInputComponent } from '../src/app/buyer-email-input/buyer-email-input.component'
import { HttpClientModule } from '@angular/common/http'
import userEvent from '@testing-library/user-event'
import { FormsModule } from '@angular/forms';

describe('Thriflora', () => {
    beforeEach( async() => {
        await render(AppComponent, {
            detectChanges: true,
            declarations: [
                RegistrationMessageComponent,
                RegistrationButtonComponent,
                CountriesSelectComponent,
                AcceptanceCheckboxComponent,
                BuyerEmailInputComponent
            ],
            imports: [HttpClientModule, FormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        })
    })

    it('does not show the registration message until be registered', () => {

        const registrationMessage = screen.queryByText('¡Perfecto, te avisaremos la primera!')

        expect(registrationMessage).toBeNull()
    })

    it('does not allow to register when the fields are not filled', () => {

        const registration = screen.getByRole('button')

        expect(registration).toHaveAttribute('disabled')
    })

    it('shows a registration message on registration', async () => {
        const buyerEmail = screen.getByRole('textbox')
        const acceptation = screen.getByRole('checkbox')

        await userEvent.type(buyerEmail, 'email@valid.com')
        await userEvent.click(acceptation)
        const registration = screen.getByRole('button')
        await userEvent.click(registration)

        const registrationMessage = screen.getByText(/Perfecto, te avisaremos la primera/s)
        expect(registrationMessage).toBeInTheDocument()
    })
})
